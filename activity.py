class Animal():
	def __init__(self):
		
		self._name = "Animal"
		self._age = 0

	# setter methods are a way to set an instance of a class attribute
	def set_name(self, name):
		self._name = name

	# getter methods are a way to retrieve a class
	def get_name(self):
		print(f"Name of Animal: {self._name}")

	def set_age(self, age):
		self._age = age

	def get_age(self):
		print(f"Age of Animal: {self._age}")

a1 = Animal()
a1.get_name()
a1.set_name("Isis")
a1.get_name()


# Test Codes
a1.get_age()
a1.set_age(15)
a1.get_age()

class Dog(Animal):
	def __init__(self, food, sound, call, name, kind):
		super().__init__()
		self._food = food
		self._sound = sound
		self._call = call
		self._name=name
		self._kind=kind

	# getters
	def get_food(self):
		print(f"Eaten {self._food}")

	def get_sound(self):
		print(f" {self._sound}")

	def get_call(self):
		print(f"Here {self._call}")

	def get_name(self):
		print(f"{self._name}")

	def get_kind(self):
		print(f"{self._kind}")

	# setters
	def set_food(self, food):
		self._food = food

	def set_sound(self, sound):
		self._sound = sound

	def set_call(self, call):
		self._call = call

	def set_name(self, name):
		self._name=name

	def set_kind(self,kind):
		self._kind=kind

	# custom method
	def get_details(self):
		print(f"{self._name} \nEaten {self._food}\n{self._sound}\n{self._call}")

# Test Codes
d1= Dog("Isis", "Dalmatian", "Here Isis", "Isis", "here Isis")
d1.set_food("Steak")
d1.set_sound("Arf! Woof! Bark!")
d1.get_details()



class Cat(Animal):
	def __init__(self, food, sound, call, name, kind):
		super().__init__()
		self._food = food
		self._sound = sound
		self._call = call
		self._name=name
		self._kind=kind

	# getters
	def get_food(self):
		print(f"Eaten {self._food}")

	def get_sound(self):
		print(f" {self._sound}")

	def get_call(self):
		print(f"Here {self._call}")

	def get_name(self):
		print(f"{self._name}")

	def get_kind(self):
		print(f"{self._kind}")

	# setters
	def set_food(self, food):
		self._food = food

	def set_sound(self, sound):
		self._sound = sound

	def set_call(self, call):
		self._call = call

	def set_name(self, name):
		self._name=name

	def set_kind(self,kind):
		self._kind=kind

	# custom method
	def get_details(self):
		print(f"{self._name} \nEaten {self._food}\n{self._sound}\n{self._call}")

# Test Codes
c1= Cat("Puss", "Persian", "Puss, come", "Tuna", "Come on!")
c1.set_food("Tuna")
c1.set_sound("Miaow! Nyaw! Nyaaaaa!")
c1.get_details()